Storage Arten
=============

# Storage Area Network (SAN)

* Maschinen bzw. Server oder Hypervisor haben Blockzugriff auf das Storage
* Wird meistens in Unternehmen eingesetzt
* Verbindungsarten
  * Fiber Channel (FC)
    * OSI-Layer 1
    * bei verwendung von FCP (middle-level to high-level layer), Ethernet (as low-level with TCP/IP + (FCP or iSCSI))
    * siehe [Options for storage connectivity (PDF)](https://www.redbooks.ibm.com/redbooks/pdfs/sg245470.pdf#G8.473361)
  * Ethernet
    * OSI-Layer 1+2
    * IP oder andere Protokolle
  * SCSI ist ein Standard, welche am meisten genutzt wird (high-level layer)

# Network-attached storage (NAS)

* ehr eine Verbindung aus Storage und Computer
* besitzt meistens eine IP und ist im LAN des Heimnetzwerks oder des kleinen Unternehmensnetzwerk verfügbar
* Remotemaschinen besitzten meistens keinen Blockzugriff, sondern nur Zugriff über Protokolle wie FTPS, ...

# Quellen
* [Wikipedia - Storage Area Network](https://en.wikipedia.org/wiki/Storage_area_network)
* [Introduction of Storage Area Networks - Red Hat Books](https://www.redbooks.ibm.com/redbooks/pdfs/sg245470.pdf)
