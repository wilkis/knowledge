Datenbanken (SQL)
===========

# Begrifflichkeiten

* Entität
  * dies ist ein **eindeutiges identifizierbares Objekt** oder **Sachverhalt** aus der **Real- oder Vorstellungswelt**
  * z.B. Brille von John Lennon, Edward Snowden
* Attribute / Eigenschaften
  * **bestimmen eine Entität**
    * können auch aus mehreren Teilen(z.B. Vorname + Nachname) zusammengesetzt werden
  * z.B. Name, Stärke, Größe
  * bekannt auch als Spalten oder Felder
* Attributswerte
  * sind die **eigentlichen Daten**
      * müssen keine Daten beinhalten (NULL)
  * z.B. John, 2, 170cm
* Entitätstyp
  * Oberbriffe einer Entität, **fasst Entitäten zusammen als Gruppe mit den selben Attributen**
    * werden als Klassen im ER-Diagramm bzw. als Tabellen wiedergespiegelt
  * z.B. Brillen, Filmcharaktere
* Tupel
  * dies ist **eine Zeile eines Entitätstypen**, welcher aus mehreren Attributswerten besteht
  * z.B. 42, The Hitchhiker's Guide to Galaxy, Douglas Adams ,1978, Ursa Minor

# Datentypen

Beispiel | Datentyp | Weiteres
-------- | -------- | --------
| **Ganzzahlen** |
42 | TINYINT |
31342 | SMALLINT |
1000000 | MEDIUMINT |
1000000000 | INT / INTEGER |
9007199254740992 | BIGINT |
| **Fließkommazahlen** |
8217499.237897 | FLOAT | Für eine kleine Anzahl von Zahlen gedacht
217349823.2942009 | DOUBLE | Für eine große Anzahl von Zahlen gedacht
1298309213.182309 | DECIMAL | Speichert die Zahlen als String
| **Datumangaben** |
2019-03-30 | DATE |
2019-03-30 16:19:51 | DATETIME |
1553959306 | TIMESTAMP | Sekunden seit dem 1.1.1970 00:00:00 UTC
| **Zeichenkette** |
Die Antwort auf | CHAR | feste Länge von 255 Zeichen
das Leben, | VARCHAR | fexible Länge von bis zu 255 Zeichen
das Universum und den ganzen Rest | TEXT | feste Länge von 65535 Zeichen
* Die Datentypen sind in manchen Datenbanksystemen anderes und weichen dem geschrieben hier eventuell ab.

# Befehlessyntax

## SELECT
Befehl | Beschreibung
------ | ------------
SELECT vorname, nachname FROM kundendaten; | Ausgabe aller Vor- und Zunamen aller Kundendaten.
SELECT * FROM rechnungen ORDER BY Summe DESC; | Ausgabe alle Attributswerte aus der Tabelle Rechnungen absteigend nach der Summe.
SELECT fName FROM fuhrpark WHERE fVorantwortliche = (SELECT Nachname FROM mitarbeiter WHERE Vorname IN (Kerstin, Hans, Greta, Dieter)); | **Subselect** :bulb: ; Ausgabe aller Fahrzeugnamen, welche von Verantwortlichen die Kerstin, Hans, Greta oder Dieter heißen geführt werden.
SELECT Namen, Vorname FROM kundendaten WHERE kVorantwortliche = (SELECT Nachname FROM mitarbeiter WHERE Alter BETWEEN 30 AND 40); | Subselect; Ausgabe aller Kundennamen, welche von Verantwortlichen zwischen 30 und 40 Jahren betreut werden.
SELECT * FROM fuhrpark WHERE fID >= 30 AND NOT Marke IN (Audi, BMW); | Ausgabe aller Fahrzeugnummern über 29 und keine Audis oder BMWs sind
SELECT Vorname, Nachname FROM kundendaten WHERE Ort LIKE '%n' AND Land IN (Deutschland, Österreich, Schweiz); | Ausgabe aller Kundennamen, welche in Orten die mit n enden und in DE, AT oder CH wohnen.
* **ORDER BY**: ASC: aufsteigend(default), DESC: absteigend

## Aggregatfunktionen
Befehl | Beschreibung
------ | ------------
SELECT COUNT(fID) FROM fuhrpark GROUP BY Marke; | Ausgabe aller Fahrzeuge im Fuhrpark nach Marken
SELECT COUNT(fID) FROM fuhrpark GROUP BY Marke HAVING AVG(km) >=30000; | Ausgabe aller Fahrzeuge im Fuhrpark nach Marken, welche über 29.999km im Durchschnitt gefahren sind.
SELECT AVG(Alter) FROM kundendaten; | Ausgabe des Durchschnittsalter aller Kunden
SELECT SUM(Dienstjahre) FROM mitarbeiter; | Gesamtdienstjahre aller Mitarbeiter_Innen
SELECT MAX(Alter) AS Lebensjahre FROM mitarbeiter WHERE Geschlecht='w'; | Ausgabe der Lebensjahre der ältesten Mitarbeiterin.
SELECT MIN(Ausgabe) AS Gesamtausgabe FROM kundendaten WHERE Land IN (Dänemark); | Ausgabe der wenigsten Ausgaben eines Kunden aus Dänemark
* AVG und SUM geben nur das Ergebnis eines nummerischen Attributs zurück.

## Datenmanipulierung
Befehl | Beschreibung
------ | ------------
DELETE FROM kundendaten WHERE Mail='douglas@adams.galaxy'; | Löschen der Datensatz mit der Mail douglas@adams.galaxy
UPDATE kundendaten SET premium='true' WHERE Vorname IN (Sina, Leon); | Setzen von Premium für alle Sinas und Leons in der Tabelle kundendaten
INSERT INTO fuhrpark  VALUES(313, '313', 'Donald Duck'); | Einfügen des 313er in die Tabelle fuhrpark

## Tabellenverwaltung
Befehl | Beschreibung
------ | ------------
CREATE fuhrpark ( fID SMALLINT, fName VARCHAR(30)); | Erstellen einer neuen Tabelle "fuhrpark" mit zwei Spalten (fID und fName)
ALTER TABLE fuhrpark ADD (fPlaetze TINYINT); | Hinzufügen des Attributs fPlaetze zu der Tabelle fuhrpark
ALTER TABLE fuhrpark DROP (fName); | Löschen des Attributs fName aus der Tabelle fuhrpark
DROP TABLE fuhrpark; | Löschen der Tabelle fuhrpark

# Beziehungen

## Begrifflichkeiten
* Primärschlüssel :key:
  * identifiziert einen Tupel (Datensatz) einer Tabelle eindeutig
  * z.B. Kundennummer (K2339ND) oder Mailadressen
  * können auch aus zusammengesetzten Attribute bestimmt werden (Name + Vorname)
* Fremdschlüssel :key:
  * *Verweist ein Attribut auf einen Datensatz in einer anderen Tabelle, hat es die Funktion eines "Fremdschlüssels".* - [informatikzentrale.de](https://www.informatikzentrale.de/_files/06datenbanken/datenbanken02_fremdschluessel.pdf)

## Kardinalitäten
* Gibt an durch wie viele Beziehungen höchstens verschiedene Entitätstypen untereinander bestehen.
* Da die Kardianlität wechselseitig auf beiden Seiten besteht, wird diese durch zwei Angaben bzw. einer Angabe an einem Entitätstyp beschrieben.

### 1:1
* "eins-zu-eins"
* z.B. Bundespräsident_In <-----> Staatsbürger_In
  * Es kann nur ein/e Staatsbürger_In Bundespräsident_In werden und das Amt des/der Bundespräsident_In kann nur von einem/einer Staatsbürger_In durchgeführt werden.

### 1:n
* "eins-zu-viele"
* z.B. Kunde <-----> Gebäude
  * Ein Kunde kann mehrere Häuser besitzen, allerdings kann ein Haus nicht mehreren Kunden gehören

### n:m
* "viele-zu-viele"
* Freelancer <-----> Unternehmen
  * Ein Freelancer kann bei mehreren Unternehmen arbeiten und ein Unternehmen kann mehrere Freelancer für sich arbeiten lassen.
* Wird oft weg normalisiert; Dabei wird eine neue Tabelle mit zwei Fremdschlüsseln aus den beiden anderen Tabellen erstellt.
  * Dabei kann der Primärschlüssel auch durch das verknüpfen zweier Fremdschlüssel entstehen.

# ToDo
* [ ] Normalform

# Quellen
* [MySQL - SQL - Grundlagen- Datentypen](https://www.peterkropff.de/site/mysql/typen.htm)
* [SQL Data Types - w3schools](https://www.w3schools.com/sql/sql_datatypes.asp)
* [SQL COUNT(), AVG() and SUM() Functions - w3schools](https://www.w3schools.com/sql/sql_count_avg_sum.asp)
* [Grundbegriffe Entität und Attribut - von Tino Hempel](https://www.tinohempel.de/info/info/datenbank/begriffe.htm)
* [Beziehungen in Datenbanken - mediencommunity](https://mediencommunity.de/system/files/Beziehungen%20in%20Datenbanken.pdf)
* [Kardinalität - von Tino Hempel](https://www.tinohempel.de/info/info/datenbank/kardinalitaet.htm)
