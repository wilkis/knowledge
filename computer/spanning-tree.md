# Spanning Tree Protocol (STP)
Spanning Tree Protocol auf Deutsch Spannbaum-Protokoll. Festgehalten in IEEE 802.1D

## Grundlagen
Das STP ist ein Verfahren um ein scheifenloses(loop-freies) Netz zu betreiben.
> redundante Wege sind relaubt, aber nur *genau einer* darf aktiv sein

> -- Guido Wessendorf

## Rapid Spanning Tree Protocol
* IEEE 802.1w
* abwärtskompatipel zu STP

## Aufbau Testnetzwerk
```
                         PC2
                          |
    Switch2 ---------- Switch3
        |                 |
        +----- Switch1 ----+
                 |
                PC1
```

## ohne Spanning Tree
Hinweis: Ethernetrahmen haben keine TTL.

### Broadcastproblem mit MAC Tabellen
1. PC1 sendet einen Broadcast.
2. Switch1 nimmt den Frame an und tragt PC1 in die MAC Tabelle ein mit dem empfangenden Switchport und schickt den Frame an alle anderen Ports raus.
3. Switch2 und Switch3 bekommen den Frame und tragen sowie Switch1 den PC1 mit deren Empfängerport in die MAC Tabelle ein. Da Switch2 und Switch3 miteinander verbunden sind, schicken diese sich gegenseitig den Broadcast zu.
4. Switch2 und Switch3 bekommen den Frame des jeweils anderen und tragen wieder PC1 mit dem Empfängerport in die MAC Tabelle. Da der Frame immer noch ein Broadcast ist, wird dieser wieder an alle andere Port rausgeschickt.
5. Nun bekommen Switch1, Switch2 und Switch3 den Frame und schicken diesen wieder an alle Ports raus.
6. Und so weiter...

### Broadcaststurm
Dies läuft ungefähr so wie das Broadcastproblem mit den MAC Tabellen. Beginnen tut es wieder mit einem Broadcast, dieser wir dann von einem Host aufgenommen und wieder als Broadcast rausgeschickt. Nach kurzer Zeit sind so viele Broadcast im Netzwerk unterwegs, das dieses zusammenbricht. :boom:

### Doppete Unicast Rahmen
1. PC1 sendet einen Frame an PC2. Dieser landet zuerst beim Switch1.
2. Switch1 weiß nicht wo PC2 ist und schickt deswegen den Frame an alle anderen Ports raus.
3. Switch2 und Switch3 nehmen den Frame auf und wissen aufgrund der MAC Tabelle unter welchen Port PC2 erreichbar ist.
4. Weil Switch3 direkt neben PC2 kommt der Frame sofort bei PC2 an. Allerdings läuft der Frame von Switch2 über Switch3. Dieser schickt den Frame sowie im Schritt 3 zu PC2.  

Somit wurden dem PC2 nun **zwei** Rahmen zugeschickt.

## Beschreibung

### Schleifenfreiheit
Dabei werden einzelne Verbindung in folgenden Fällen abgeschaltet.  

1. Verbindung vom Switch in den selben Switch
```
     Switch1
      |   |
      +-X-+
```
an diesen Fall werden die entsprechenden Ports deaktiviert. Dies entsteht durch Dusseligkeit oder nicht vorhandener Kenntnis.

2. Bandbreitenbündelung
```
      Switch1
       |  |
       |  X
       |  |
      Switch2
```
Hier wurden doppelte Verkabelung verbaut um Bandbreite zu bündeln. Allerdings verwenden die verschiedenen Switchhersteller Verfahren womit dies wieder aufgehoben wird. Bei Spanning Tree werden 2x 1 Gbit/s als 2 Gbit/s wahrgenommen.

3. Redundanz
```
      Switch2 ----X--- Switch3
          |               |
          +--- Switch1 ---+
```
Diese Vernetzung wird unteranderen gewählt, um Ausfall verkraften zu können.

### Die Wahlen
Beim Spanning Tree Verfahren braucht man einen Meister, die Root-Bridge.  
Die Root-Bridge muss gewählt werden. Da es noch keine KI-Switches haben, wird die Root-Bridge unter bestimmten Eigenschaften gewählt. Der Switch mit der niedrigsten BridgeID gewinnt. Die BridgeID (8 Oktette) besteht aus:
* Bridge Prioirty (4 Bits)
  * Dieser Wert ist frei wählbar. Somit kann ein bestimmter Switch als Root-Bridge bevorzugt werden. Dieser Wert liegt zwischen 4096 - 61140 in 4096 Schritten.
* Extend System ID (12 Bits)
  * Die Extend System ID errechnet sich durch das entsprechende VLAN.
* MAC Adresse (6 Oktette)
  * Dieser Wert ist per seh nicht veränderbar und oft haben die älteren und schwächen Switche eine niedrige MAC-Adresse weil die Hersteller diese aufsteigend vergeben.

#### Berechnung Bridge Prioirty (2 Otektte)
```
+---------------------+--------------------------------------+
| Bridge Prioirty     | Extend System ID (VLAN)              |
+-----+-----+----+----+----+----+---+---+---+--+--+--+-+-+-+-+
|32768|16484|8192|4096|2048|1024|512|256|128|64|32|16|8|4|2|1|
+-----+-----+----+----+----+----+---+---+---+--+--+--+-+-+-+-+
|  1  |  0  | 1  | 0  | 0  | 0  | 0 | 0 | 0 |0 |0 |0 |0|0|0|1|
+-----+-----+----+----+----+----+---+---+---+--+--+--+-+-+-+-+
```
Somit beträgt die Bridge Prioirty 40961.

#### Wahlvorgang
Am Anfang des Wahlvorgangs geht jeder Switch davon aus selber die Root-Bridge zu sein. Dann fangen die Switches an BDPUs (Bridge Data Protocol Units) hin und her zu schicken. Denn anderen Switchen wird mitgeteilt welche BridgeID man hat und welche RootBridgeID(bei ersten Senden beides gleich) der Switch verwendet. Dieser Vorgang wird solange wiederholt bis alle Switche sich ausgetauscht haben und eine Root-Bridge auserkoren wurde.  
Sollten weitere Switche dem Netzwerk beitreten, kann es unter Umständen eine neue Root-Bridge geben und dies könnte eventuell zur Änderungen in der Topologie führen. Verhindern könnte man dies durch den RootGuard, dieser deaktiviert den Switchport beim Versand von BDPUs.

### Kosten
Damit nun der möglichst beste Weg zur Root-Bridge bestimmt werden kann, gibt es eine Kostentabellen.  
Standardwerte:

Bitübertragungsrate | Kostenpunkte
------ | -----
4 Mbit/s | 250 	
10 Mbit/s | 100
16 Mbit/s | 62
100 Mbit/s | 19
1 Gbit/s | 4
2 Gbit/s | 3
10 Gbit/s | 2
Sollte eine Verbindung aus zwei gebündete Leitung mit je 1 Gbit/s bestehen, werden 2 Gbit/s raus und 3 Kostenpunkte berechnet.
Außerdem haben 8 Gbit/s genau so viele Kostenpunkte wie 2 Gbit/s, für solche Situation lassen sich die Kosten pro Port einzeln konfigurieren.
Sollte die Pfadkosten bei zwei Bridges gleich sein,

### Rollen
Sollte sich eine Root-Bridge gefunden haben, muss sich jeder Switchport einer Rolle zuordnen.

Nach 802.1d:

* Root Port
  * Dies ist der Port der zur Root-Bridge führt.
* Designated Port
  * Dies ist der Port der vom Root-Bridge weg geht.
* Blocking Port
  * Alle ankommenden Inhalte werden verworfen.

Es gibt für RSTP (RapidSTP)(802.1w), eine andere Zusammensetzung der Zustände:
* Root
* Designated
  * Dies trifft wie in 802.1D auf die Ports der Root-Bridge zu aber auch auf deinen Port welcher als Backup an einem weiteren Switch aktiv ist
* Alternate
  * Empfängt im Falle Rahmen vom "Backup"-Designated-Port
* Backup
  *

### Zustände
Neben den Rollen kann ein Switchport auch einen der nachfolgende Zustände besitzten.

* Forwading
  * Normaler Modus
  * Sendet und empfangt Frames
* Listening
  * Wartet auf neue Informationen über BDPU
  * Erwartet eine Änderung in den Blocking Zustand
  * Keine Weiterleitung von Frames
  * Keine Eintragung in die MAC address table
* Learning
  * Keine Weiterleitung von Frames
  * Lernen der Adressen in die MAC address Tabele
* Blocking
  * Userdaten werden weder gesendet noch empfangen
  * BPDUs werden empfangen
  * kann in den Forwarding Zuständ versetzt werden
  * Verhindert Loops(Schleifen)
* Disabled
  * Verwurf samtlicher Frames

Bei RSTP (802.1w) gibt es nur die Zustände: Disabled, Learing und Forwarding

## Quellen
* CiscoWiki Version 5  
* deutsche Wikipeia - [Spanning Tree Protokoll (de)](https://de.wikipedia.org/wiki/Spanning_Tree_Protocol)
* englische Wikipedia - [Spanning Tree Protocol (en)](https://en.wikipedia.org/wiki/Spanning_Tree_Protocol)
* Admin-Magazin 03/2014 - [Wie organisiert Spanning Tree ein Ethernet-Netzwerk?](http://www.admin-magazin.de/Das-Heft/2014/03/Wie-organisiert-Spanning-Tree-ein-Ethernet-Netzwerk) - von Hannes Kasparick
* Firewall.cx - [Spanning Tree Protocol: Bridge ID, Priority, System ID Extension & Root Bridge Election Process](http://www.firewall.cx/networking-topics/protocols/spanning-tree-protocol/1054-spanning-tree-protocol-root-bridge-election.html) - von Arani Mukherjee
* Universät Münster [STP, RSTP und MSTP](https://www.uni-muenster.de/ZIV.GuidoWessendorf/vorlesung/STP/STP.pdf) - von Guido Wessendorf
* Netzwerkblog - [Wahl der Root-Bridge beim Spanning-Tree](http://www.netzwerkblog.com/wahl-der-root-bridge-beim-spanning-tree/)

## Lizenz
[CC-BY](https://creativecommons.org/licenses/by/4.0/deed.de) Marvin Wilke
