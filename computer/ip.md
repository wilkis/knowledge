Internet Protocol (IP)
=================

* OSI-Layer 3

# IPv4
* Protokollversion 4
* 32 bit Adresse
* sie besteht aus einem Netz- und Hostanteil
  * z.B. IP Adresse: 13.42.192.190 Subnetzmaske: 255.255.255.192

Adressen |in Zahlen | -   | -   |  -
-------- | -------  | --- | --- | ---
IP Adresse (dezimal) | 13 | 42 | 192 | 190
Subnetzmaske (dezimal) | 255 | 255 | 255 | 192
IP Adresse (binär) | 00001101 | 00101010 | 11000000 | 10111110
Subnetzmaske (binär) | 11111111 | 11111111 | 11111111 | 11000000
Netzanteil | xxxxxxxx | xxxxxxxx | xxxxxxxx | xx------
Hostanteil | -------- | -------- | -------- | --xxxxxx
Netzadresse (binär) | 00001101 | 00101010 | 11000000 | 1000000
Broadcastadresse (binär) | 00001101 | 00101010 | 11000000 | 10111111
Netzadresse (dezimal) | 13 | 42 | 192 | 128
Broadcastadresse (dezimal) | 13 | 42 | 192 | 191
Hostadresse von  (dezimal) | 13 | 42 | 192 | 129
Hostadresse bis (dezimal) | 13 | 42 | 192 | 190

* in der obrigen Tabelle wurden diese Schritte angewendet:
 1. Die Oktette(Teile) werden codiert in Binärzahlen.
 2. Alle 1en in der binären Subnetzmaske definieren den Netzanteil.
 3. Übring bleibt der Hostanteil.
 4. Im letzten Oktett werden die Binärzahlen der IP Adresse und der Subnetzmaske "UND" verknüpft und man erhalt die Netzadresse, welche das Netzwerk identifiziert.
 5. Zur Berechnung der Broadcastadresse wird die Netzadresse übernommen und der Hostanteil mit 1en gefüllt.
 6. Die nachfolgene Adresse der Netzadresse ist die erste Host-IP im vorhandenen Subnetz.
 7. Die letzte Adresse vor der Broadcastadresse ist die letzte Host-IP im vorhandenen Subnetz.

## Typen
Es gibt drei verschiendene Typen von IPv4 Adresse:
* Unicast
  * Nur zu einem bestimmten Computer
* Multicast
  * Zu einer Gruppe von bestimmten Computern
* Broadcast
  * Zu allen Computern im Netzwerk

## Klassen

Bits | Format | Klasse | von | bis
---- | ------ | ------ | --- | ---
0 | 7 bits für den Netzanteil, 24 bits für den Hostanteil | A | 0.0.0.0 | 127.255.255.255
10 | 14 bits für den Netzanteil, 16 bits für den Hostanteil | B | 128.0.0.0 | 191.255.255.255
110 | 21 bits für den Netzanteil, 8 bits für den Hostanteil | C | 192.0.0.0 | 223.255.255.255
111 | Multicast | D | 224.0.0.0 | 239.255.255.255
1110 | für Erweiterungen | E | 240.0.0.0 | 255.255.255.255

* relevant für die Klassen niedrig, weil Classless Inter-Domain Routing (CIDR) eingeführt wurde und IPv6 immer mehr benutzt wird.

## Private Netzwerke
* Aufgrund der großen Vergabe von IPv4 Adresse und immer wachsende Anteil von Computern im Internet, sind oft innerhalb von Organisationen/Haushalten private IP Adressen zu finden.
* definiert in rfc1918
* private Netzwerke
  * 10.0.0.0/8
  * 172.16.0.0/12
  * 192.168.0.0/24

## Header
```
 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|Version|  IHL  |Type of Service|          Total Length         |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|         Identification        |Flags|      Fragment Offset    |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|  Time to Live |    Protocol   |         Header Checksum       |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                       Source Address                          |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                    Destination Address                        |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                    Options                    |    Padding    |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
```

# IPv6
* 128 bit Adresse
* im Vergleich zu IPv4
  * vereinfachter Header
  * mehr IP Adressen
  * Kennzeichung eines Flows
  * Authentifikation (ESP - IPsec, siehe rfc4303)
  * Privatsphäre  (Privacy Extension, siehe rfc4941)
  * keine Verletzung des Ende-zu-Ende Prinzip

* ICMPv6
  * ICMP Neighbour Discovery
    * Duplicate IP check
    * Ersatz für ARP

* Autodiscovery
  * EUI-64
    * ![Paste MAC into EUI-64](files/ip_EUI-64.png)

## Typen
Es gibt drei verschiendene Typen von Adressen:
* Unicast
  * Nur an ein bestimmtes Interface
* Anycast
  * an das nächstliegende Interface mit einer bestimmten Adresse
* Multicast
  * an alle Interfaces mit der selben Adresse

Es gibt keine Broadcastadresse mehr, dafür sind nun die Multicastadressen zuständig.

## Adressen
* Alle führenden Nullen dürfen gekürzt werden
  * 2a01:4f8:fe18:182a:2b2:1c:f:1
* Mehrere Nullen dürfen nur einmal gekürzt werden
  * z.B. 2a01:4f8:1c0c:4943::1

### Spezial Adressen

IP  | Bedeutung
--- | ---------
::/128 | nicht spezifiziert
::1/128 | Loopback
ff00::/8 | Multicast
fe80::/10 | Link-Local Unicast
fc00::/7 | Unique Local Unicast (private IP Adressen)
0:0:0:0:0:ffff::/96 | IPv4 mapped IPv6 (letzte Zeichen sind IPv4 Adressen)
2000::/3 | Global Unicast
2001::/16 | Vom Provider
2001:db8::/32 | Dokumentationszwecke
2a00:0000::/12 | Global Unicast
... | ...

## Header
```
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|Version| Traffic Class |           Flow Label                  |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|         Payload Length        |  Next Header  |   Hop Limit   |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                                                               |
+                                                               +
|                                                               |
+                         Source Address                        +
|                                                               |
+                                                               +
|                                                               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                                                               |
+                                                               +
|                                                               |
+                      Destination Address                      +
|                                                               |
+                                                               +
|                                                               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
```
* Version (4 bit)
  * beschreibt die Protokollversion
  * default: 6
* Traffic Class (8 bit)
  * QoS
* Flow Label (20 bit)
  * definiert welches Pakete zu welchen Flow gehören
* Payload Length (16 bit)
  * Länge der Daten im IPv6 Paket
* Next Header (8 bit)
  * definiert weitere Optionen und wo sie zu finden sind
* Hop Limit (8 bit)
  * Definiert an wie vielen Hops(Routern) es max. geroutet wird, bevor es verworfen wird
* Source Address (128 bit)
  * Quell IP
* Destination Address (128 bit)
  * Ziel IP

# Quellen
* [INTERNET PROTOCOL - rfc701](https://tools.ietf.org/html/rfc791)
* [Classless Inter-Domain Routing (CIDR) - rfc1519](https://tools.ietf.org/html/rfc1519)
* [Address Allocation for Private Internets - rfc1918](https://tools.ietf.org/html/rfc1918)
* [Internet Protocol, Version 6 (IPv6) Specification - rfc8200](https://tools.ietf.org/html/rfc8200)
* [IP Version 6 Addressing Architecture - rfc4291](https://tools.ietf.org/html/rfc4291)
* [IPv6 Global Unicast Address Assignments - IANA](https://www.iana.org/assignments/ipv6-unicast-address-assignments/ipv6-unicast-address-assignments.xhtml)
* [IPv6 Wikipedia](https://de.wikipedia.org/wiki/IPv6)
* [IPv6 im Jahre 2018](https://media.ccc.de/v/froscon2018-2242-ipv6_im_jahre_2018)
