Firewall
========

# Paketfilter
* OSI Layer 3 + 4
* es gibt ein Regelwerk und diese gelten für alle Pakete, welche an diesem Computer verarbeitet werden.

```
Chain INPUT (policy ACCEPT)
target     prot opt source               destination
ACCEPT     tcp  --  anywhere             anywhere         tcp dpt:http
DROP       all  --  anywhere             anywhere         tcp dpt:ssh
ACCEPT     all  --  3.28.38.0/24         anywhere         tcp dpt:ftp

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination
```
Ausgabe von iptables -L
* Eingehende, Weiterleitende und Ausgehende Verbindungen werden allgemein erlaubt
* Verwerfen und Modifizieren werden auch als Policy erlaubt
* Eingehende Verbindungen auf Port 80 (HTTP) über TCP werden von allen IPs erlaubt
* Eingehende Verbindungen auf Port 22 (SSH) über jegliches Protokoll wir verworfen
* Eingehende Verbindungen auf Port 21 (FTP) über jegliches Protokoll wird nur von der IP 3.28.38.0/24 erlaubt

Oder auch so dargestellt:

Accept / Deny | Protocol | Source | Destination | Source Port | Destination Port | Interface | Direction
------------- | -------- | ------ | ----------- | ----------- | ---------------- | --------- | ---------
Deny | IP | Any | 42.19.0.0/16 | Any | 23 | eth1 | IN
Allow | TCP | 192.168.178.0/24 | Any | Any | 80 | eth24 | OUT
Allow | TCP | 192.168.178.0/24 | Any | Any | 443 | eth24 | OUT
Allow | TCP | Any | 192.168.178.0/24 | 80 | Any | eth24 | IN
Allow | TCP | Any | 192.168.178.0/24 | 443 | Any | eth24 | IN
Allow | UDP | Any | Any | Any | 53 | eth3 | OUT
Allow | UDP | Any | Any | 53 | Any | eth3 | IN

## Vor- und Nachteile
* Verbindungen in die geschützte Zone werden standardmäßig offen gehalten.
* Wird bei mehren Regeln unübersichtlich (unübersichtlich = unsicher)

# Stateful Packet Inspection (SPI)
* OSI Layer 3 + 4

## Warum SPI?
Wenn Computer aus der geschützten Zone einen Request in das Internet machen, muss bei den meisten Verbindungen ein Antwort erfolgen. Deswegen muss in der FW der Port offen bleiben, auf welchen der Request ins Internet ging. Über diesen kann dann der Computer im Internet seine Antwort schicken. Beim Paketfilter müssten dazu die Ports dauerhaft für alle offen bleiben. Dies hat allerdings zur Folge das sich die Angriffsfläche der geschützten Zone erhöht.

## SPI
Von Außen werden alle Verbindungen in die geschützte Zone verboten.

Wenn nun ein Computer aus der geschützten Zone einen Request (HTTPS, ...) an einem Computer im Internet stellt, wird kurzfristig der Quellport von Außen für die Antwort des Internetcomputers freigeschaltet.

Der offene Port kann je nach Konfiguration nach einer gewissen Zeit(timeout) wieder geschlossen werden.
Optional können nur Verbindungen von der Ziel IP erlaubt werden.

# Gute Links
* [iptables comic](https://twitter.com/b0rk/status/1054056111626686465)

# Quellen
* N/A
