Gute Links :thumbsup:
=====================

# IT

## Unterbrechungsfreie Stromversorgung (USV/UPS)
* [USV Grundlagen - Thomas Krenn Wiki](https://www.thomas-krenn.com/de/wiki/USV_Grundlagen)
* [USV - Unterbrechungsfreie Stromversorgung - Elektronik-Kompendium.de](https://www.elektronik-kompendium.de/sites/grd/0812171.htm)
* [Unterbrechungsfreie Stromversorgung - Wikipedia](https://de.wikipedia.org/wiki/Unterbrechungsfreie_Stromversorgung)

## Serielle und parallele Übertragung
* [Serielle und parallele Übertragung - TU Berlin](https://www.cfd.tu-berlin.de/Lehre/EDV1/skripte/alles/node163.html)

## Routing
* [Dynamische Routingprotokolle Aufzucht und Pflege - OSPF - media.ccc.de](https://media.ccc.de/v/froscon2018-2243-dynamische_routingprotokolle_aufzucht_und_pflege_-_ospf)
* [**RIP** - Dynamische Routingprotokolle Aufzucht und Pflege - OSPF - media.ccc.de](https://media.ccc.de/v/froscon2018-2243-dynamische_routingprotokolle_aufzucht_und_pflege_-_ospf#t=297)

## SLAs
* [Service-Level-Agreement - Wikipedia](https://de.wikipedia.org/wiki/Service-Level-Agreement)
>  indem [...] Leistungsumfang, Reaktionszeit und Schnelligkeit der Bearbeitung genau beschrieben werden

## Arbeitsspeicher
* [Puffer, Register, Fehlerkorrektur - Speicher-Links und FAQ - c't Redaktion](https://www.heise.de/ct/Redaktion/ciw/speicher.html#Puffer)

# Projektmanagement

## Projektdefinition
* [6 Merkmale, an denen du ein Projekt erkennst - Projekte-leicht-gemacht.de](https://projekte-leicht-gemacht.de/blog/definitionen/6-merkmale-projekt/)
* [Projekt – Definition: Was ist ein Projekt? - Agile Master](https://www.agile-master.de/projekt-definition/)

# Wirtschaft

## Rechtsformen
* [Tabelle Rechtsformen - IHK Rhein Neckar](https://www.rhein-neckar.ihk24.de/blob/maihk24/recht/downloads/fallback1423211064252/938568/0d994d06057f67b4d5a124220276fcd9/Tabelle_Unternehmensformen-data.pdf)
* [Rechtsformen in DE - Wikipedia](https://de.wikipedia.org/wiki/Liste_von_Rechtsformen_von_Unternehmen_in_Deutschland)
* [Rechtsform - DAA Wirtschaftslexikon](https://media.daa-pm.de/ufv_wirtschaftslexikon/Html/R/Rechtsform.htm)

## Angebots -und Nachfragekurve
* [Marktgleichgewicht vorlesung.info](https://www.vorlesungen.info/node/1268)
* [:unlock: Angebot / Nachfrage - bwlhelfer.de](http://bwlhelfer.de/html/angebot_und_nachfrage.html)

## Marktformen
* [Marktformen - Bundeszentrale für politische Bildung](https://www.bpb.de/nachschlagen/lexika/lexikon-der-wirtschaft/20075/marktformen)
* [Marktformen - Marktgleichgewicht - Wikipedia](https://de.wikipedia.org/wiki/Marktgleichgewicht#Marktformen)

## Verkäufer- und Käufermarkt
* [Verkäufermarkt- Bundeszentrale für politische Bildung](https://www.bpb.de/nachschlagen/lexika/lexikon-der-wirtschaft/20997/verkaeufermarkt)
* [Käufermarkt- Bundeszentrale für politische Bildung](https://www.bpb.de/nachschlagen/lexika/lexikon-der-wirtschaft/19894/kaeufermarkt)
* [Verkäufer- und Käufermarkt - Wirtschaftswiki der FH Aachen](https://www.wirtschaftswiki.fh-aachen.de/index.php?title=K%C3%A4ufermarkt)
