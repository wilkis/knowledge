Webentwicklung
==============

# html
```
<!DOCTYPE html>
<html>
  <head>
    <title>Browser's Title</title>
    <meta charset="UTF-8">
  </head>
  <body>
    <a href="https://marvinw.xyz">Klicken Sie hier!!11!</a>
    <h1>Überschribt h1</h1>
    <center>Zentral</center>
    <div>
      Ein
      <br>
      Block
    </div>
    <ul>
      <li>Runder Punkt1</li>
      <li>Runder Punkt2</li>
    </ul>
    <ol>
      <li>erster Punkt</li>
      <li>zweiter Punkt</li>
    </ol>
  </body>
</html>
```

# php
```
<?php
echo 'Dies ist eine Ausgabe!';

$host = "localhost";
$user = "superstore-user";
$pawd = "r3xvLXF3GAjO";
$base = "superstore";

// connect to database
$conn = new mysqli($host, $user, $pawd, $base);

$query = "INSERT INTO fuhrpark VALUES(313, '313', 'Donald Duck')";

// execute SQL query
$conn->exec($query);

// close connection to database
mysqli_close($conn);
?>
```

# JavaScript
```
<script type="text/javascript">
  var x = 300 + 13;
  window.alert(21 * 2);
</script>
```

# GET vs POST

## Beschreibungen

### GET
* Daten werden an den String der Internetadressen angehängt.
* z.B. https://www.youtube.com/watch?v=9bZkp7q19f0&t=3m
* Anzahl der Zeichen können durch Browser/Proxy/Webserver begrenzt sein
* Der HTTP Request kann von allen gelesen werden, welche Zugriff auf Host/Proxy/Webserver besitzen.
* Einfaches Debugging

### POST
* schreibt Daten in den HTTP Header
* nur in Verbindung mit TLS für Passwörter geeignet


## im Quellcode
##### x.html
```
<form action = "calculation.php" method = "GET">
<form action = "calculation.php" method = "POST">
  <table>
    <tr>
      <th>Name</th>
      <th>Wert</th>
    </tr>
    <tr>
      <td>Name: </td>
      <td><input type="text" name="name" /></td>
    </tr>
    <td>
      <td>Lieblingsfarbe: </td>
      <td><input type="text" name="color" /></td>
    </tr>
    <tr>
      <td><input type="reset" /></td>
      <td><input type="submit" /></td>
    </tr>
  </table>
</form>
```
##### calculation.php
```
<?php
  $name = $_GET["name"];
  $color = $_POST["color"];
  echo "Die Lieblingsfarbe von $name ist $color";
?>
```


# Quellen
* [PHP Connect to MySQL - w3schools](https://www.w3schools.com/php/php_mysql_connect.asp)
* [RFC2616 - Section 3.2.1](https://tools.ietf.org/html/rfc2616#section-3.2.1)
