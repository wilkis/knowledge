OSI Schichtenmodell
=========================
* lang: Open Systems Interconnection Model

# OSI Layer
Schicht | English (Original) | Deutsch
------- | ------------------ | -------
7 | Application | Anwendung
6 | Presentation | Präsentation
5 | Session | Sitzung
4 | Transport | Transport
3 | Network | Vermittlung
2 | Data Link | Sicherung
1 | Physical | Bitübertragung

## PDU / Dienste
* PDU = Protocol Data Units

Schicht | English (Original) | PDU | Dienste
------- | ------------------ | --- | -------
7 | Application | - | HTTP, FTP
6 | Presentation | - | TLS
5 | Session | - | SOCKS
4 | Transport | Segments (Segmente) | TCP, UDP
3 | Network | Packets (Pakete) | IP, VRRP, MPLS
2 | Data Link | Frames (Rahmen) | ARP
1 | Physical | Bits | -

# TCP/IP Model
```
+---------+----------------+--------------+
| Schicht |     TCP/IP     |     OSI      |
+---------+----------------+--------------+
|         |                | Application  |
|         |                +--------------+
|    4    | Application    | Presentation |
|         |                +--------------+
|         |                | Session      |
+---------+----------------+--------------+
|    3    | Host-to-Host   | Transport    |
+---------+----------------+--------------+
|    2    | Internet       | Network      |
+---------+----------------+--------------+
|         |                | Data Link    |
|    1    | Network Access +--------------+
|         |                | Physical     |
+---------+----------------+--------------+
```

# Quellen
* [Information technology – Open Systems Interconnection – Basic Reference Model: The basic model](https://www.itu.int/ITU-T/recommendations/rec.aspx?rec=2820)
