My Personal Knowledge Base
==========================

:uk:

If you find some bugs or spelling errors, [create](https://codeberg.org/wilkis/knowledge/compare/master...master) a pull request or [write](https://codeberg.org/wilkis/knowledge/issues/new) an issue.

:de:

Wenn du Fehler oder Rechtsschreibfehler in diesem Repo findet, [erstelle](https://codeberg.org/wilkis/knowledge/compare/master...master) einen Pull Request oder [schrieb](https://codeberg.org/wilkis/knowledge/issues/new) ein Issue.
